﻿//#define SUPPORT_PARALLEL

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.WindowsAPICodePack.Shell;

namespace ThumbnailSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();

            sw.Start();

#if SUPPORT_PARALLEL
            Parallel.For (0, 400, i =>
#else
            for (var i = 0; i < 400; ++i)
#endif
            {
                var inputDir = args[0];
                var outputDir = args[1];

                // input file path
                var inputFilePath = Path.Combine(inputDir, "input " + (i + 1) + ".pdf");
                // output file path
                var outputFilePath = Path.Combine(outputDir, "output " + (i + 1) + ".png");

                using (var shellFile = ShellFile.FromFilePath(inputFilePath))
                {
                    var shellThumbnail = shellFile.Thumbnail;
                    shellThumbnail.FormatOption = ShellThumbnailFormatOption.ThumbnailOnly;
                    shellThumbnail.RetrievalOption = ShellThumbnailRetrievalOption.Default;

                    using (var bitmap = shellThumbnail.LargeBitmap)
                    {
                        bitmap.Save(outputFilePath, System.Drawing.Imaging.ImageFormat.Png);
                    }
#if SUPPORT_PARALLEL
                });
#else
                }
#endif
            }

            sw.Stop();

            Console.WriteLine("Elapsed: {0}", sw.Elapsed);
        }
    }
}
